import React, { Component } from 'react'
import { storeProducts, detailProduct } from './data';

const ProductContext = React.createContext();
//Provider
//Consumer 

class ProductProvider extends Component {
    state = {
        products: [],
        detailProduct: detailProduct,
        cart: [],
        modalOpen: false,
        modalProduct: detailProduct,
        cartSubtotal:0,
        carTax:0,
        cartTotal:0
    }

    componentDidMount() {
        this.setProducts();
    }

    setProducts = () => {
        let tempProducts = [];
        storeProducts.forEach(item => {
            const singleItem = {...item};
            tempProducts = [...tempProducts, singleItem];
        });
        this.setState(() => {
            return { products: tempProducts };
        });
    }

    getItem = (id) => {
        const product = this.state.products.find(item => item.id === id);
        return product;
    }

    handleDetail = (id) => {
        //console.log('hello from detail');
        const product = this.getItem(id);
        this.setState(() => {
            return { detailProduct: product };
        })
    }

    addToCart = (id) => {
        //console.log(`hello from add to cart ${id}`);
        let tempProducts = [...this.state.products];
        const index = tempProducts.indexOf(this.getItem(id));
        const product = tempProducts[index];
        product.inCart = true;
        product.count = 1;
        const price = product.price;
        product.total = price;
        
        this.setState(() => {
            return {products:tempProducts, cart:[...this.state.cart, product] };
        }, () => {
            console.log(this.state);
        })
    }

    openModal = id => {
        const product = this.getItem(id);
        this.setState(() => {
            return {modalProduct:product, modalOpen:true}
        })
    }

    closeModal = () => {
        this.setState(() => {
            return {modalOpen:false}
        })
    }

    increment = (id) => {
        console.log('this is increment method');
    }

    decrement = (id) => {
        console.log('this is decrement method');
    }

    removeItem = (id) => {
        console.log('item removed');
    }

    clearCart = () => {
        console.log('cart was cleared');
    }

    /* just a tester method to test the right way to copy the value not as refernce
    tester = () => {
        console.log('State products :', this.state.products[0].inCart);
        console.log('Data products :', storeProducts[0].inCart);

        const tempProducts2 = [...this.state.products];
        tempProducts2[0].inCart = true;

        this.setState(() => {
            return {products:tempProducts2}
        }, () => {
            console.log('State products :', this.state.products[0].inCart);
            console.log('Data products :', storeProducts[0].inCart);
        })
    }*/

    render() {
        return (
            <ProductContext.Provider
            value={{
                ...this.state,
                handleDetail:this.handleDetail,
                addToCart:this.addToCart,
                openModal: this.openModal,
                closeModal: this.closeModal,
                increment: this.increment,
                decrement: this.decrement,
                removeItem: this.removeItem,
                clearCart: this.clearCart

            }}
            >
                {/*<button onClick={this.tester}>test me</button> used for tester method to test*/}
                {this.props.children}
            </ProductContext.Provider>
        )
    }
}

const ProductConsumer = ProductContext.Consumer;

export {ProductProvider, ProductConsumer};